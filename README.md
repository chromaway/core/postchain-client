# Postchain-client

Postchain-client is a Kotlin client for making queries and transactions from an application.

## Setup

To use the library, you'll need to add these package registries since it's not available on Maven Central. 
The specific steps for adding the URL may differ based on your chosen build tool.

For Maven add this to your `pom.xml`:

```xml
<project>
    <dependencies>
        <dependency>
            <groupId>net.postchain.client</groupId>
            <artifactId>postchain-client</artifactId>
        </dependency>
    </dependencies>
    
    <repositories>
        <repository>
            <id>chromia-parent</id>
            <name>Chromia parent GitLab Registry</name>
            <url>https://gitlab.com/api/v4/projects/50818999/packages/maven</url>
        </repository>
        <repository>
            <id>postchain</id>
            <name>Postchain GitLab Registry</name>
            <url>https://gitlab.com/api/v4/projects/32294340/packages/maven</url>
        </repository>
        <repository>
            <id>postchain-client</id>
            <name>Postchain Client GitLab Registry</name>
            <url>https://gitlab.com/api/v4/projects/46288950/packages/maven</url>
        </repository>
    </repositories>
</project>
```

For Gradle, add this to your `build.gradle.kts`:

```kotlin
repositories {
    maven("https://gitlab.com/api/v4/projects/50818999/packages/maven")
    maven("https://gitlab.com/api/v4/projects/32294340/packages/maven")
    maven("https://gitlab.com/api/v4/projects/46288950/packages/maven")
}

dependencies {
    implementation("net.postchain.client:postchain-client")
}
```

## Usage

### Initializing the Client

```kotlin
import net.postchain.client.config.PostchainClientConfig
import net.postchain.crypto.Secp256K1CryptoSystem
import net.postchain.client.core.PostchainClient
import net.postchain.client.impl.PostchainClientProviderImpl
import net.postchain.client.request.EndpointPool
import net.postchain.common.BlockchainRid
import net.postchain.common.hexStringToByteArray
import net.postchain.common.tx.TransactionStatus
import net.postchain.crypto.KeyPair

fun main() {
    val cryptoSystem = Secp256K1CryptoSystem()
    val bcRid: BlockchainRid = BlockchainRid.buildFromHex("...") // The blockchain RID of the chain (can be found in the logs when P.C. server starts)
    val pubKey0 = "...".hexStringToByteArray() // This must be a real public key
    val privKey0 = "...".hexStringToByteArray() // This must be a real private key
    val keyPair0 = KeyPair(pubKey0, privKey0)
    val sigMaker0 = cryptoSystem.buildSigMaker(keyPair0)
    val endpointPool = EndpointPool.singleUrl("http://127.0.0.1:7740") // Running P.C. server on localhost
    val psClient: PostchainClient = PostchainClientProviderImpl().createClient(
            PostchainClientConfig(bcRid, endpointPool, listOf(keyPair0))
    )
}
```

### Queries
```kotlin
psClient.query("hello_world", GtvFactory.gtv(mapOf()))
```

### Transactions
Create a basic transaction, sign it, and send it.
```kotlin
val txBuilder = psClient.transactionBuilder()
txBuilder.addOperation("nop") // Operation "nop" without arguments
txBuilder.sign(sigMaker0) // Sign it

val result = txBuilder.post()
when (result.status) {
    TransactionStatus.WAITING -> println("TX has been put into the TX queue of the Postchain server")
    TransactionStatus.REJECTED -> println("TX most likely wrong number of args")
    TransactionStatus.CONFIRMED -> println("TX has been included in a block")
    TransactionStatus.UNKNOWN -> println("Investigate")
}  
```
Build a transaction containing operation with arguments, sign it, and send it.
```kotlin
val txBuilder = psClient.transactionBuilder()
txBuilder.addOperation("set_name", GtvFactory.gtv(name))
//addNop makes tx unique so one can send same operation more than one time
txBuilder.addNop()
txBuilder.sign(sigMaker0) // Sign it
val result = txBuilder.post()
```

# Chromia client

The Chromia client is an alternative to the Postchain client and can be used to more easily communicate with Chromia networks. When initialized it will lookup and connect to system nodes of the network.

## Setup

For Maven add this to your `pom.xml`:

```xml
<project>
    <dependencies>
        <dependency>
            <groupId>net.postchain.client</groupId>
            <artifactId>chromia-client</artifactId>
        </dependency>
    </dependencies>
    
    <repositories>
        <repository>
            <id>chromia-parent</id>
            <name>Chromia parent GitLab Registry</name>
            <url>https://gitlab.com/api/v4/projects/50818999/packages/maven</url>
        </repository>
        <repository>
            <id>postchain</id>
            <name>Postchain GitLab Registry</name>
            <url>https://gitlab.com/api/v4/projects/32294340/packages/maven</url>
        </repository>
        <repository>
            <id>postchain-client</id>
            <name>Postchain Client GitLab Registry</name>
            <url>https://gitlab.com/api/v4/projects/46288950/packages/maven</url>
        </repository>
    </repositories>
</project>
```

For Gradle, add this to your `build.gradle.kts`:

```kotlin
repositories {
    maven("https://gitlab.com/api/v4/projects/50818999/packages/maven")
    maven("https://gitlab.com/api/v4/projects/32294340/packages/maven")
    maven("https://gitlab.com/api/v4/projects/46288950/packages/maven")
}

dependencies {
    implementation("net.postchain.client:postchain-client")
}
```

## Example usage

To create a client and wait for a transaction to be anchored:

```kotlin
val chromiaClient = StandardChromiaClient("http://127.0.0.1:7740")
chromiaClient.awaitAnchoredTx(
        BlockchainRid.buildFromHex("335C75E08AFAC7D6678263F1A13D5AFED9CD009344B6349107D7CEEA3A40EA08"),
        TxRid("3DE7FC7BCF6DAF2FFD8564D46D73F42C069818DDC249835535AD50D8D9270FF3"))
```
