package net.postchain.client.impl

import org.http4k.core.Status
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class StrategyTest {

    @Test
    fun `Status UNKNOWN_HOST is server failure`() {
        assertTrue(isServerFailure(Status.UNKNOWN_HOST))
    }

    @Test
    fun `Status CONNECTION_REFUSED is server failure`() {
        assertTrue(isServerFailure(Status.CONNECTION_REFUSED))
    }

    @Test
    fun `Status BAD GATEWAY is server failure`() {
        assertTrue(isServerFailure(Status.BAD_GATEWAY))
    }
}
