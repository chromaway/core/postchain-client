package net.postchain.client.core

@JvmInline
value class TxRid(val rid: String)
