package net.postchain.client.core

@JvmInline
value class BlockRid(val rid: String)
