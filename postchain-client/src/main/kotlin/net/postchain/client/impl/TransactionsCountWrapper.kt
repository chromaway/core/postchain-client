package net.postchain.client.impl

internal data class TransactionsCountWrapper(val transactionsCount: Long)