package net.postchain.client.impl

internal data class TransactionWrapper(val tx: String)
