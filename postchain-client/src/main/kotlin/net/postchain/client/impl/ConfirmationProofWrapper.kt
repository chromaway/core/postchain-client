package net.postchain.client.impl

internal data class ConfirmationProofWrapper(val proof: String)
