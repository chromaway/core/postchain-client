package net.postchain.client.request

enum class EndpointHealthStatus {
    Reachable,
    Unreachable
}
