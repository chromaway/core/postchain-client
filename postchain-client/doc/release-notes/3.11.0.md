# RELEASE NOTES 3.11.0 (2023-06-13)

* Optionally keep track of size in `TransactionBuilder` to avoid overflowing max transaction size
